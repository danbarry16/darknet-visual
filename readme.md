# Darknet Visual

*Create network visualizations for Darknet network configurations.*

## Example

Conversion example:

```
java -jar dist/darknet-visual.jar xYOLO.cfg xYOLO.svg
```

Expected output (actual [SVG](doc/xYOLO.svg)):

![xYOLO visualization](doc/xYOLO.png)

**NOTE:** More details about xYOLO [here](https://arxiv.org/abs/1910.03159).
