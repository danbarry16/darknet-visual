package b.dv;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Config.java
 *
 * Parse the configuration file and allow it to be visualized.
 **/
public class Config{
  /**
   * Layer.Config.java
   *
   * Basic description of a network layer for the purpose of visualization.
   **/
  public class Layer{
    private HashMap<String, String> props;

    /**
     * Layer()
     *
     * Create a new network layer.
     *
     * @param name The name or type of the network layer.
     **/
    public Layer(String name){
      props = new HashMap<String, String>();
      addKeyValue("name", name);
    }

    /**
     * getShortName()
     *
     * Get the short name of the layer.
     *
     * @return The layer name,
     **/
    public String getShortName(){
      String name = get("name", "");
      switch(name){
        case "convolutional" :
          if(get("xnor", 0) == 0){
            return "conv";
          }else{
            return "convXB";
          }
        case "maxpool" :
          return "max";
        case "yolo" :
        case "route" :
        case "upsample" :
        default :
          if(name.length() <= 8){
            return name;
          }else{
            return name.substring(0, 8);
          }
      }
    }

    /**
     * addKeyValue()
     *
     * Add a key value pair describing some properties of this layer.
     *
     * @param key The key for the variable.
     * @param val The value to be stored.
     **/
    public void addKeyValue(String key, String val){
      /* Clean-up for comparison */
      key = key.trim();
      val = val.trim();
      props.put(key, val);
    }

    /**
     * addKeyValueInt()
     *
     * Add a key value pair describing some properties of this layer.
     *
     * @param key The key for the variable.
     * @param val The value to be stored.
     **/
    public void addKeyValueInt(String key, int val){
      addKeyValue(key, Integer.toString(val));
    }

    /**
     * addKeyValueDouble()
     *
     * Add a key value pair describing some properties of this layer.
     *
     * @param key The key for the variable.
     * @param val The value to be stored.
     **/
    public void addKeyValueDouble(String key, double val){
      addKeyValue(key, Double.toString(val));
    }

    /**
     * get()
     *
     * Get a property given a key.
     *
     * @param key The key layer variable is stored under.
     * @param def The default value encase value cannot be found.
     * @return The value, otherwise default.
     **/
    public String get(String key, String def){
      return props.get(key) != null ? props.get(key) : def;
    }

    /**
     * get()
     *
     * Get a property given a key.
     *
     * @param key The key layer variable is stored under.
     * @param def The default value encase value cannot be found.
     * @return The value, otherwise default.
     **/
    public int get(String key, int def){
      String res = get(key, null);
      return res != null ? Integer.parseInt(res) : def;
    }

    /**
     * get()
     *
     * Get a property given a key.
     *
     * @param key The key layer variable is stored under.
     * @param def The default value encase value cannot be found.
     * @return The value, otherwise default.
     **/
    public double get(String key, double def){
      String res = get(key, null);
      return res != null ? Double.parseDouble(res) : def;
    }

    /**
     * toString()
     *
     * Get a printable String version of this layer.
     *
     * @return The String version of this layer.
     **/
    @Override
    public String toString(){
      String temp = "[" + get("name", "name") + "]\n";
      /* Loop over additional key value pairs */
      for(String key : props.keySet()){
        temp += key + "=" + props.get(key) + "\n";
      }
      return temp;
    }
  }

  private int width;
  private int height;
  private int channels;
  private HashMap<String, String> props;
  private ArrayList<Layer> layers;

  /**
   * Config()
   *
   * Load the configuration file, ready for parsing.
   *
   * @param filename The location of the configuration file.
   **/
  public Config(String filename){
    /* Setup internal variables */
    width = 1000;
    height = 1000;
    channels = 3;
    props = new HashMap<String, String>();
    layers = new ArrayList<Layer>();
    /* Parse file line by line */
    File file = new File(filename);
    try{
      Scanner scan = new Scanner(file);
      while(scan.hasNextLine()){
        /* Get line */
        String line = scan.nextLine();
        /* Remove comments */
        line = cleanLine(line);
        /* If line is blank, skip */
        if(line.length() <= 0){
          continue;
        }
        /* Check if we are processing a label */
        if(line.charAt(0) == '['){
          String label = line.substring(1, line.length() - 1);
          if(!label.equals("net")){
            layers.add(new Layer(label));
          }
        }else if(line.contains("=")){
          String[] var = line.split("=");
          /* There should only be two parts to a variable assignment */
          if(var.length != 2){
            System.err.println("Variable had " + var.length + " parts");
          }
          String key = var[0].trim();
          String val = var[1].trim();
          /* Make sure there is a layer to add to */
          if(layers.size() <= 0){
            switch(key){
              case "width" :
                width = Integer.parseInt(val);
                break;
              case "height" :
                height = Integer.parseInt(val);
                break;
              case "channels" :
                channels = Integer.parseInt(val);
                break;
              default :
                props.put(key, val);
                break;
            }
          }else{
            layers.get(layers.size() - 1).addKeyValue(key, val);
          }
        }else{
          System.err.println("Unknown network line '" + line + "'");
        }
      }
    }catch(IOException e){
      /* Do nothing */
    }
    /* Process first layer in network */
    layers.get(0).addKeyValueInt("width", width);
    layers.get(0).addKeyValueInt("height", height);
    /* Process network layers */
    for(int x = 0; x < layers.size() - 1; x++){
      /* Get current layer properties */
      Layer thisLayer = layers.get(x);
      String name = thisLayer.get("name", "name");
      int width =   thisLayer.get("width", 0);
      int height =  thisLayer.get("height", 0);
      int filters = thisLayer.get("filters", 0);
      int size =    thisLayer.get("size", 0);
      int stride =  thisLayer.get("stride", 1);
      /* Setup next layer */
      Layer nextLayer = layers.get(x + 1);
      switch(name){
        case "convolutional" :
        case "maxpool" :
        case "yolo" :
        case "route" :
        case "shortcut" :
          nextLayer.addKeyValueInt("width", width / stride);
          nextLayer.addKeyValueInt("height", height / stride);
          break;
        case "upsample" :
          nextLayer.addKeyValueInt("width", width * stride);
          nextLayer.addKeyValueInt("height", height * stride);
          break;
        default :
          System.err.println("Unknown layer type '" + name + "'");
          nextLayer.addKeyValueInt("width", width / stride);
          nextLayer.addKeyValueInt("height", height / stride);
          break;
      }
    }
  }

  /**
   * cleanline()
   *
   * Remove comments from line and any existing white space.
   *
   * @param line The line to be cleaned.
   * @return The cleaned line.
   **/
  private String cleanLine(String line){
    line = line.trim();
    if(line.indexOf('#') >= 0){
      line = line.substring(0, line.indexOf('#'));
    }
    return line;
  }

  /**
   * getLayer()
   *
   * Get a layer from the neural network.
   *
   * @param x The index of the layer to be retrieved.
   * @return The requested layer, otherwise NULL.
   **/
  public Layer getLayer(int x){
    if(x < layers.size()){
      return layers.get(x);
    }else{
      return null;
    }
  }

  /**
   * numLayers()
   *
   * The number of layers in the network.
   *
   * @return Number of layers in the network, otherwise -1.
   **/
  public int numLayers(){
    return layers.size();
  }

  /**
   * pad()
   *
   * Pad and align a given String.
   *
   * @param s The string to be padded.
   * @param c The padding character.
   * @param l The target length of the string.
   * @param a Left align if true, otherwise right align.
   * @return The padded string,
   **/
  private String pad(String s, char c, int l, boolean a){
    while(s.length() < l){
      if(a){
        s += c;
      }else{
        s = c + s;
      }
    }
    return s;
  }

  /**
   * toDesc()
   *
   * Produce a String description of the network, similar to YOLO's output
   * after loading a network.
   *
   * @return The String description of the network.
   **/
  public String[] toDesc(){
    String[] temp = new String[layers.size()];
    int prevFilters = channels;
    for(int x = 0; x < layers.size(); x++){
      Layer l = layers.get(x);
      temp[x] = l.getShortName();
      int filters = l.get("filters", 0);
      if(filters != 0){
        temp[x] += "," + filters;
      }else{
        temp[x] += ",";
      }
      /* Treat some layers different */
      switch(l.get("name", "")){
        case "shortcut" :
          int from = Integer.parseInt(l.get("from", "0"));
          if(from < 0){
            from += x;
          }
          temp[x] += "," + from;
          break;
        case "route" :
          prevFilters = 0;
          String[] routes = l.get("layers", "0").split(",");
          for(int i = 0; i < routes.length; i++){
            int r = Integer.parseInt(routes[i].trim());
            int z = r < 0 ? (x + r) : r;
            temp[x] += "," + z;
            int f = 0;
            while(f == 0){
              f = layers.get(z).get("filters", 0);
              --z;
            }
            prevFilters += f;
          }
          break;
        case "yolo" :
          /* Do nothing */
          break;
        case "upsample" :
          temp[x] += "," + l.get("stride", 1);
          /* Input size */
          temp[x] += "," + l.get("width", 1)  +
                     "," + l.get("height", 1) +
                     "," + prevFilters;
          /* Update only if filters provided */
          if(filters > 0){
            prevFilters = filters;
          }
          /* Output size */
          if(x + 1 < layers.size()){
            temp[x] += "," + layers.get(x + 1).get("width", 1)  +
                       "," + layers.get(x + 1).get("height", 1) +
                       "," + prevFilters;
          }else{
            temp[x] += "," + l.get("width", 1)  +
                       "," + l.get("height", 1) +
                       "," + prevFilters;
          }
          break;
        case "convolutional" :
        case "maxpool" :
          /* Kernel size */
          temp[x] += "," + l.get("size",  1) +
                     "," + l.get("size",  1) +
                     "," + l.get("stride", 1);
          /* Input size */
          temp[x] += "," + l.get("width", 1)  +
                     "," + l.get("height", 1) +
                     "," + prevFilters;
          /* Update only if filters provided */
          if(filters > 0){
            prevFilters = filters;
          }
          /* Output size */
          if(x + 1 < layers.size()){
            temp[x] += "," + layers.get(x + 1).get("width", 1)  +
                       "," + layers.get(x + 1).get("height", 1) +
                       "," + prevFilters;
          }else{
            temp[x] += "," + l.get("width", 1)  +
                       "," + l.get("height", 1) +
                       "," + prevFilters;
          }
          break;
        default :
          System.err.println("Unknown layer type at " + x);
          break;
      }
    }
    return temp;
  }

  /**
   * toDescription()
   *
   * Produce a String description of the network, similar to YOLO's output
   * after loading a network.
   *
   * @return The String description of the network.
   **/
  public String toDescription(){
    String[] desc = toDesc();
    String temp = "layer     filters    size              input                output\n";
    for(int x = 0; x < desc.length; x++){
      String[] d = desc[x].split(",");
      temp += pad(x + "", ' ', 4, false) + " ";
      temp += pad(d[0], ' ', 8, true) + " ";
      if(d.length > 1){
        temp += pad(d[1], ' ', 4, false) + " ";
      }
      switch(d[0]){
        case "shortcut" :
          break;
        case "route" :
          for(int z = 2; z < d.length; z++){
            temp += d[z] + " ";
          }
          break;
        case "yolo" :
          break;
        case "upsample" :
          /* Size */
          temp += pad(d[2] + "x", ' ', 12, false) + "  ";
          /* Input */
          temp += pad(d[3], ' ', 4, false) + " x ";
          temp += pad(d[4], ' ', 4, false) + " x ";
          temp += pad(d[5], ' ', 4, false) + "  ->  ";
          /* Output */
          temp += pad(d[6], ' ', 4, false) + " x ";
          temp += pad(d[7], ' ', 4, false) + " x ";
          temp += pad(d[8], ' ', 4, false);
          break;
        case "conv" :
        case "convXB" :
        case "max" :
          /* Size */
          temp += pad(d[2], ' ', 2, false) + " x ";
          temp += pad(d[3], ' ', 2, false) + " / ";
          temp += pad(d[4], ' ', 2, false) + "  ";
          /* Input */
          temp += pad(d[5], ' ', 4, false) + " x ";
          temp += pad(d[6], ' ', 4, false) + " x ";
          temp += pad(d[7], ' ', 4, false) + "  ->  ";
          /* Output */
          temp += pad(d[ 8], ' ', 4, false) + " x ";
          temp += pad(d[ 9], ' ', 4, false) + " x ";
          temp += pad(d[10], ' ', 4, false);
          break;
        default :
          System.err.println("Unknown layer type '" + d[0] + "' at " + x);
          break;
      }
      temp += "\n";
    }
    return temp;
  }

  /**
   * toString()
   *
   * Generate a String representation of this network configuration.
   *
   * @return The String representation of this network.
   **/
  @Override
  public String toString(){
    String temp = "[net]\n";
    temp += "width=" + width + "\n";
    temp += "height=" + height + "\n";
    /* Loop network properties */
    for(String key : props.keySet()){
      temp += key + "=" + props.get(key) + "\n";
    }
    /* Loop network layers */
    for(int x = 0; x < layers.size(); x++){
      temp += "\n" + layers.get(x).toString();
    }
    return temp;
  }
}
