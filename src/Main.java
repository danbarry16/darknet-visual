package b.dv;

/**
 * Main.java
 *
 * Parse the command line arguments and execute the program as appropriate.
 **/
public class Main{
  private Visual vis;

  /**
   * main()
   *
   * The main entry point into the program. Create an instance of the Main
   * object and pass on the command line parameters.
   *
   * @param args The command line arguments.
   **/
  public static void main(String[] args){
    new Main(args);
  }

  /**
   * Main()
   *
   * Parse the command line arguments and decide on the next execution of the
   * program.
   *
   * @param args The command line arguments.
   **/
  public Main(String[] args){
    vis = new Visual3D();
    String cfg = null;
    String out = null;
    /* Loop over arguments */
    for(int x = 0; x < args.length; x++){
      switch(args[x]){
        case "-h" :
        case "--help" :
          x = help(args, x);
          break;
        case "-v" :
        case "--visual" :
          x = visual(args, x);
          break;
        default :
          /* If it's the second to last parameter, we have a config */
          if(x == args.length - 2){
            cfg = args[x];
          /* If it's the last parameter, we have an output file */
          }else if(x == args.length - 1){
            out = args[x];
          }else{
            System.err.println("Unknown argument '" + args[x] + "'");
            /* Quit program */
            return;
          }
          break;
      }
    }
    /* If only one last parameter, it was a config */
    if(cfg == null){
      cfg = out;
      out = null;
    }
    /* Do we have enough to start the main program? */
    if(cfg != null && vis != null){
      Config config = new Config(cfg);
      SVG output = vis.process(config);
      if(out != null){
        output.save(out);
      }else{
        System.out.println("No output provided, this was a dry run");
      }
    }else{
      System.out.println("Not able to process, not enough parameters");
    }
  }

  /**
   * help()
   *
   * Display the command line arguments and then quit.
   *
   * @param args The command line arguments being searched.
   * @param x The current offset into the command line parameters.
   * @return The new offset into the command line parameters.
   **/
  private int help(String[] args, int x){
    System.out.println("darknet-visual [OPT] [CFG] [OUT]");
    System.out.println("darknet-visual [OPT] [OUT]");
    System.out.println("");
    System.out.println("  OPTions");
    System.out.println("");
    System.out.println("    -h  --help    Display this help");
    System.out.println("    -v  --visual  Set the type of visual");
    System.out.println("                    <STR> The type of visual");
    System.out.println("                      3D  3D coloured visual");
    System.out.println("");
    System.out.println("  ConFiG");
    System.out.println("");
    System.out.println("    A standard network configuration file for the");
    System.out.println("    Darknet framework.");
    System.out.println("");
    System.out.println("  OUTput");
    System.out.println("");
    System.out.println("    The SVG file to output the visual to.");
    /* Quit program */
    return args.length;
  }

  /**
   * visual()
   *
   * Set the type of visual to be used for the network.
   *
   * @param args The command line arguments being searched.
   * @param x The current offset into the command line parameters.
   * @return The new offset into the command line parameters.
   **/
  private int visual(String[] args, int x){
    if(++x < args.length){
      switch(args[x]){
        case "3D" :
          vis = new Visual3D();
          break;
        default :
          System.err.println("Unknown visual '" + args[x] + "'");
          break;
      }
    }
    return x;
  }
}
