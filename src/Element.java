package b.dv;

/**
 * Element.java
 *
 * The basic structure of an SVG element.
 **/
public interface Element{
  /**
   * addStyle()
   *
   * Add a single (key,value) pair to be added.
   *
   * @param key The key name.
   * @param val The variable to be added to the key.
   **/
  public void addStyle(String key, String val);

  /**
   * addStyles()
   *
   * Add one or more points to the polygon.
   *
   * @param ss The set of (key,value) pairs to be added.
   **/
  public void addStyles(String[][] ss);

  /**
   * minX()
   *
   * Minimum X position.
   *
   * @return The minimum X location.
   **/
  public double minX();

  /**
   * minY()
   *
   * Minimum Y position.
   *
   * @return The minimum Y location.
   **/
  public double minY();

  /**
   * maxX()
   *
   * Maximum X position.
   *
   * @return The maximum X location.
   **/
  public double maxX();

  /**
   * maxY()
   *
   * Maximum Y position.
   *
   * @return The maximum Y location.
   **/
  public double maxY();
}
