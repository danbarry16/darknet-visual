package b.dv;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * ElementPolygon.java
 *
 * The basic structure of an SVG polygon.
 **/
public class ElementPolygon implements Element{
  private HashMap<String, String> styles;
  private ArrayList<double[]> points;

  /**
   * ElementPolygon()
   *
   * Create a polygon element.
   **/
  public ElementPolygon(){
    styles = new HashMap<String, String>();
    points = new ArrayList<double[]>();
  }

  public void addStyle(String key, String val){
    styles.put(key, val);
  }

  public void addStyles(String[][] ss){
    for(int x = 0; x < ss.length; x++){
      if(ss[x].length == 2){
        addStyle(ss[x][0], ss[x][1]);
      }else{
        System.err.println("Invalid (key,value) pair for style");
      }
    }
  }

  /**
   * addPoint()
   *
   * Add a single point to the polygon.
   *
   * @param p The (x,y) point to be added.
   **/
  public void addPoint(double[] p){
    if(p.length == 2){
      points.add(p);
    }else{
      System.err.println("Invalid number of 2D points");
    }
  }

  /**
   * addPoints()
   *
   * Add one or more points to the polygon.
   *
   * @param ps The set of (x,y) points to be added.
   **/
  public void addPoints(double[][] ps){
    for(int x = 0; x < ps.length; x++){
      addPoint(ps[x]);
    }
  }

  public double minX(){
    double r = points.get(0)[0];
    for(int x = 1; x < points.size(); x++){
      if(points.get(x)[0] < r){
        r = points.get(x)[0];
      }
    }
    return r;
  }

  public double minY(){
    double r = points.get(0)[1];
    for(int x = 1; x < points.size(); x++){
      if(points.get(x)[1] < r){
        r = points.get(x)[1];
      }
    }
    return r;
  }

  public double maxX(){
    double r = points.get(0)[0];
    for(int x = 1; x < points.size(); x++){
      if(points.get(x)[0] > r){
        r = points.get(x)[0];
      }
    }
    return r;
  }

  public double maxY(){
    double r = points.get(0)[1];
    for(int x = 1; x < points.size(); x++){
      if(points.get(x)[1] > r){
        r = points.get(x)[1];
      }
    }
    return r;
  }

  /**
   * toString()
   *
   * Convert this element to a String.
   *
   * @return The string representation of this element.
   **/
  @Override
  public String toString(){
    String out = "<polygon points=\"";
    /* Add points */
    for(int x = 0; x < points.size(); x++){
      if(x != 0){
        out += " ";
      }
      out += points.get(x)[0] + "," + points.get(x)[1];
    }
    out += "\" style=\"";
    /* Add styles */
    for(String key : styles.keySet()){
      out += key + ":" + styles.get(key) + ";";
    }
    out += "\" />";
    return out;
  }
}
