package b.dv;

/**
 * Visual.java
 *
 * An interface for how a network may be visualized.
 **/
public interface Visual{
  /**
   * process()
   *
   * Convert a network configuration into an SVG visualization.
   *
   * @param cfg The network configuration to be processed.
   * @return The SVG output.
   **/
  public SVG process(Config cfg);
}
