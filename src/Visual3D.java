package b.dv;

/**
 * Visual3D.java
 *
 * A staggered 3D representation of the neural network.
 **/
public class Visual3D implements Visual{
  private static final int SPACING = 4;
  private String[][] STYLES_SHORTCUT = new String[][]{
    {"fill",      "#4F4"},
    {"stroke",    "#000"},
    {"stroke-width", "1"}
  };
  private String[][] STYLES_ROUTE = new String[][]{
    {"fill",      "#FCC"},
    {"stroke",    "#000"},
    {"stroke-width", "1"}
  };
  private String[][] STYLES_YOLO = new String[][]{
    {"fill",      "#F6F"},
    {"stroke",    "#000"},
    {"stroke-width", "1"}
  };
  private String[][] STYLES_UPSAMPLE = new String[][]{
    {"fill",      "#FF0"},
    {"stroke",    "#000"},
    {"stroke-width", "1"}
  };
  private String[][] STYLES_CONV = new String[][]{
    {"fill",      "#44F"},
    {"stroke",    "#000"},
    {"stroke-width", "1"}
  };
  private String[][] STYLES_CONVXB = new String[][]{
    {"fill",      "#0FF"},
    {"stroke",    "#000"},
    {"stroke-width", "1"}
  };
  private String[][] STYLES_MAX = new String[][]{
    {"fill",      "#F80"},
    {"stroke",    "#000"},
    {"stroke-width", "1"}
  };

  public SVG process(Config cfg){
    SVG svg = new SVG();
    /* Get CSV version of network */
    String[] desc = cfg.toDesc();
    /* Process each layer of network */
    double w = SPACING;
    double h = SPACING;
    double pwh = w * h;
    double xOff = 0;
    for(int x = 0; x < desc.length; x++){
      String[] ds = desc[x].split(",");
      String[][] styles = null;
      double d = SPACING;
      switch(ds[0]){
        case "shortcut" :
          styles = STYLES_SHORTCUT;
          break;
        case "route" :
          styles = STYLES_ROUTE;
          break;
        case "yolo" :
          styles = STYLES_YOLO;
          break;
        case "upsample" :
          styles = STYLES_UPSAMPLE;
          break;
        case "conv" :
          styles = styles == null ? STYLES_CONV :   styles;
        case "convXB" :
          styles = styles == null ? STYLES_CONVXB : styles;
        case "max" :
          styles = styles == null ? STYLES_MAX :    styles;
          w = Math.pow(Double.parseDouble(ds[5]), 0.66) * SPACING;
          h = Math.pow(Double.parseDouble(ds[6]), 0.66) * SPACING;
          d = Math.pow(Double.parseDouble(ds[7]), 0.66) * SPACING;
          break;
        default :
          System.err.println("Unknown layer type '" + ds[0] + "' at " + x);
          break;
      }
      /* Add network layer if one found */
      if(styles != null){
        if(pwh < w * h){
          xOff += SPACING * 4;
        }
        pwh = w * h;
        Element[] elems = genLayer(w, h, d, xOff, styles);
        for(Element e : elems){
          svg.add(e);
        }
        xOff += d + SPACING;
      }
    }
    /* TODO: Produce network key. */
    return svg;
  }

  /**
   * genLayer()
   *
   * Generate a layer representation.
   *
   * @param w Width of the layer.
   * @param h Height of the layer.
   * @param d Depth of the layer.
   * @param x The layer offset.
   * @param styles The layer styling.
   * @return A series of SVG elements representing the network layer.
   **/
  private Element[] genLayer(double w, double h, double d, double x, String[][] styles){
    ElementPolygon[] e = new ElementPolygon[3];
    /* Forward face */
    e[0] = new ElementPolygon();
    e[0].addStyles(styles);
    e[0].addPoints(new double[][]{
      {(x + d) - (w / 2), -h + (w / 2)},
      {(x + d),          -h},
      {(x + d),           0},
      {(x + d) - (w / 2), 0 + (w / 2)},
    });
    /* Side face */
    e[1] = new ElementPolygon();
    e[1].addStyles(styles);
    e[1].addPoints(new double[][]{
      {(x    ) - (w / 2), -h + (w / 2)},
      {(x + d) - (w / 2), -h + (w / 2)},
      {(x + d) - (w / 2), 0 + (w / 2)},
      {(x    ) - (w / 2), 0 + (w / 2)},
    });
    /* Top face */
    e[2] = new ElementPolygon();
    e[2].addStyles(styles);
    e[2].addPoints(new double[][]{
      {(x + d),           -h},
      {(x    ),           -h},
      {(x    ) - (w / 2), -h + (w / 2)},
      {(x + d) - (w / 2), -h + (w / 2)},
    });
    return e;
  }
}
