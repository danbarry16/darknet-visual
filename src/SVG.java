package b.dv;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 * SVG.java
 *
 * Handle the raw elements of building the SVG.
 **/
public class SVG{
  private ArrayList<Element> elements;

  /**
   * SVG()
   *
   * Create new SVG.
   **/
  public SVG(){
    elements = new ArrayList<Element>();
  }

  /**
   * add()
   *
   * Add an element to the SVG.
   *
   * @param e The element to be added.
   * @return A reference to this SVG object.
   **/
  public SVG add(Element e){
    elements.add(e);
    return this;
  }

  /**
   * save()
   *
   * Save this SVG to disk.
   *
   * @param filename The filename to save the SVG as.
   **/
  public void save(String filename){
    try{
      Files.write(
       Paths.get(filename),
       toString().getBytes(StandardCharsets.UTF_8)
     );
    }catch(IOException e){
      System.err.println("Unable to save SVG '" + filename + "'");
    }
  }

  /**
   * toString()
   *
   * Generate a String representation of this object.
   *
   * @return A String representation of this object.
   **/
  @Override
  public String toString(){
    /* Calculate view */
    double minX = elements.get(0).minX();
    double minY = elements.get(0).minY();
    double maxX = elements.get(0).maxX();
    double maxY = elements.get(0).maxY();
    for(Element e : elements){
      if(e.minX() < minX){
        minX = e.minX();
      }
      if(e.minY() < minY){
        minY = e.minY();
      }
      if(e.maxX() > maxX){
        maxX = e.maxX();
      }
      if(e.maxY() > maxY){
        maxY = e.maxY();
      }
    }
    double width = maxX - minX;
    double height = maxY - minY;
    String out = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" +
                 "<svg width=\"" + width + "\" height=\"" + height +
                   "\" viewBox=\"" + minX + " " + minY + " " + width + " " + height +
                   "\" xmlns=\"http://www.w3.org/2000/svg\">\n";
    for(Element e : elements){
      out += "  " + e.toString() + "\n";
    }
    out += "</svg>";
    return out;
  }
}
